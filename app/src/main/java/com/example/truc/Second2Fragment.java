package com.example.truc;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.example.truc.databinding.FragmentSecond2Binding;

public class Second2Fragment extends Fragment {

    private FragmentSecond2Binding binding;
    private Uri imageUri =null;
    private ActivityResultLauncher<String> galleryLauncher = registerForActivityResult(
            new ActivityResultContracts.GetContent(), result -> {
                if (result != null) {
                    // Do something with the selected image URI
                    binding.imageView.setImageURI(result);
                    imageUri =result;
                    AddActivity.contactToAdd.setUri(result);
                    Log.d("DebugDb","equal: "+(Uri.parse(result.toString())));
                }
            });

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        binding = FragmentSecond2Binding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*Uri uri=savedInstanceState.getParcelable("imageUri");
        if(uri!=null){
            binding.imageView.setImageURI(uri);
        }*/

        binding.choosePhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Use the ActivityResultLauncher to open the gallery
                galleryLauncher.launch("image/*");
            }
        });

        binding.buttonSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Navigate to the previous fragment
                NavHostFragment.findNavController(Second2Fragment.this)
                        .navigate(R.id.action_Second2Fragment_to_First2Fragment);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save any state data you want to persist across fragment recreation
        // For example, you might want to save the image URI
        if(imageUri!=null){
            outState.putParcelable("imageUri",imageUri);
        }
    }

}
