package com.example.truc;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.io.InputStream;
import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> {

    private List<Contact> contactList;
    private Context context;

    // Constructeur prenant la liste des contacts
    public ContactAdapter(List<Contact> contactList,Context context) {
        this.contactList = contactList;
        this.context=context;
    }

    // ViewHolder pour chaque élément de vue de contact
    public class ContactViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewNom;
        public TextView textViewPrenom;
        public ImageView imageView;
        public Contact contact;
        Context context;

        public ContactViewHolder(View itemView) {
            super(itemView);
            textViewNom = itemView.findViewById(R.id.text_view_nom);
            textViewPrenom = itemView.findViewById(R.id.text_view_prenom);
            imageView = itemView.findViewById(R.id.image_view_contact);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*DatabaseHelper databaseHelper=new DatabaseHelper(itemView.getContext());
                    databaseHelper.deleteContact(contact);
                    Log.d("deleteDb",contact+" to delete");*/
                    Intent intent = new Intent(context,AddActivity.class);
                    AddActivity.ContactId=contact.getId();
                    First2Fragment.ContactId=contact.getId();
                    context.startActivity(intent);
                }
            });

        }
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_list_item, parent, false);


        return new ContactViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        Contact contact = contactList.get(position);

        // Mettez à jour les éléments de vue avec les données du contact
        holder.textViewNom.setText(contact.getNom());
        holder.textViewPrenom.setText(contact.getPrenom());
        holder.contact =contact;
        holder.context =context;
        Log.d("DebugDb","adapter recieve : "+contact.getUri());
        if(contact.getUri()!=null&&isUriValid(context,Uri.parse(contact.getUri()))){
            holder.imageView.setImageURI(Uri.parse(contact.getUri()));
            Log.d("DebugDb","adapter recieve : "+contact.getUri());
        }

        // Vous pouvez ajouter d'autres éléments de vue et gérer les interactions ici
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }
    public boolean isUriValid(Context context, Uri uri) {
        if (uri == null) {
            return false;
        }

        ContentResolver contentResolver = context.getContentResolver();
        try {
            // Attempt to open the URI to check if it's valid
            InputStream inputStream = contentResolver.openInputStream(uri);
            if (inputStream != null) {
                inputStream.close();
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}