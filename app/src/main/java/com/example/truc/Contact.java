package com.example.truc;

import android.content.Context;
import android.net.Uri;
import android.provider.ContactsContract;

import androidx.annotation.NonNull;

public class Contact {
    private long id; // L'identifiant unique du contact dans la base de données
    private String nom;
    private String prenom;
    private String pseudo;
    private String adresse;
    private String numeroTelephone;
    private String uri;

    // Constructeur
    public Contact(String nom, String prenom, String pseudo, String adresse, String numeroTelephone,String uri) {
        this.nom = nom;
        this.prenom = prenom;
        this.pseudo = pseudo;
        this.adresse = adresse;
        this.numeroTelephone = numeroTelephone;
        this.uri=uri;
    }
    public Contact(){}

    // Méthodes d'accès aux propriétés
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getNumeroTelephone() {
        return numeroTelephone;
    }

    public void setNumeroTelephone(String numeroTelephone) {
        this.numeroTelephone = numeroTelephone;
    }
    public void setUri(@NonNull Uri uri){
        this.uri=uri.toString();
    }
    public void setUri(String uri){
        this.uri=uri;
    }

    public String getUri() {
        return this.uri;
    }
    public long push(Context context){
        DatabaseHelper helper =new DatabaseHelper(context);
        long x= helper.addContact(this);
        helper.close();
        return x;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", pseudo='" + pseudo + '\'' +
                ", adresse='" + adresse + '\'' +
                ", numeroTelephone='" + numeroTelephone + '\'' +
                ", uri='" + uri + '\'' +
                '}';
    }
}