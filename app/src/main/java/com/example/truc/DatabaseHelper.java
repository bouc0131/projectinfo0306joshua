package com.example.truc;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "contacts.db";
    private static final int DATABASE_VERSION = 1;

    // Constructeur
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // Créez vos tables ici
        String createTableQuery = "CREATE TABLE contacts (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "nom TEXT," +
                "prenom TEXT," +
                "pseudo TEXT," +
                "adresse TEXT," +
                "uri TEXT," +
                "numeroTelephone TEXT);";
        db.execSQL(createTableQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Handle database upgrades here
        if (oldVersion < 2) {
            // Add the 'uri' column if upgrading from version 1 to 2
            db.execSQL("ALTER TABLE contacts ADD COLUMN uri TEXT;");
        }
    }
    public long addContact(@NonNull Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.d("DebugDb","a");

        ContentValues values = new ContentValues();
        values.put("nom", contact.getNom());
        values.put("prenom", contact.getPrenom());
        values.put("pseudo", contact.getPseudo());
        values.put("adresse", contact.getAdresse());
        values.put("numeroTelephone", contact.getNumeroTelephone());
        values.put("uri", contact.getUri()); // Add the URI field
        Log.d("DebugDb","b");
        // Insert the new row, and return the primary key value of the new row
        return db.insert("contacts", null, values);
    }
    @SuppressLint("Range")
    public List<Contact> getAllContacts() {
        List<Contact> contactList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM contacts";
        Cursor cursor = db.rawQuery(selectQuery, null);
        Log.d("DebugDb","cursor number count"+cursor.getCount());

        if (cursor.moveToFirst()) {
            do {
                // Create a Contact object and populate it with data from the cursor
                Contact contact = new Contact();
                contact.setId(cursor.getInt(cursor.getColumnIndex("_id")));
                contact.setNom(cursor.getString(cursor.getColumnIndex("nom")));
                contact.setPrenom(cursor.getString(cursor.getColumnIndex("prenom")));
                contact.setPseudo(cursor.getString(cursor.getColumnIndex("pseudo")));
                contact.setAdresse(cursor.getString(cursor.getColumnIndex("adresse")));
                contact.setNumeroTelephone(cursor.getString(cursor.getColumnIndex("numeroTelephone")));
                contact.setUri(cursor.getString(cursor.getColumnIndex("uri")));

                // Add the contact to the list
                contactList.add(contact);
                Log.d("DebugDb",contact.toString());
            } while (cursor.moveToNext());
        }

        // Close the cursor and return the contact list
        cursor.close();
        return contactList;
    }
    public void deleteContact(long id) {
        // Assuming your table name is "contacts"
        String querry="DELETE FROM contacts WHERE _id = " + id + ";";
        Log.d("delete",querry);
        getWritableDatabase().execSQL(querry);
    }
    @SuppressLint("Range")
    public Contact getContact(long contactId) {
        List<Contact>  list = getAllContacts();
        for (Contact contact:list){
            if (contact.getId()==contactId)
                return contact;
        }
        return null;
    }

}