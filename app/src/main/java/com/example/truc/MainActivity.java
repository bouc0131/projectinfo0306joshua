package com.example.truc;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DatabaseHelper dbHelper =new DatabaseHelper(this);
        List<Contact> contacts = dbHelper.getAllContacts();
        Log.d("DebugDb","contacts number: "+contacts.size());
        if (contacts.size()==0){
            /*contacts.add(new Contact("CHAPELET","Joshua","Cafenoir","somewhere ig","06 66 66 66 66",null));
            contacts.add(new Contact("truc","truc","Cafenoir","somewhere ig","06 66 66 66 66",null));
            contacts.add(new Contact("machin","machin","Cafenoir","somewhere ig","06 66 66 66 66",null));
            contacts.add(new Contact("things","things","Cafenoir","somewhere ig","06 66 66 66 66",null));
            contacts.add(new Contact("voisin","voisin","Cafenoir","somewhere ig","06 66 66 66 66",null));*/
        }
        ContactAdapter adapter=new ContactAdapter(contacts,this);
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setAdapter(adapter);
        Button btn_view =findViewById(R.id.btn_add_contact);
        btn_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(MainActivity.this,AddActivity.class);
                startActivity(intent);
            }
        });

    }
}