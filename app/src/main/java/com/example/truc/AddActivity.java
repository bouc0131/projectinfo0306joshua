package com.example.truc;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;

import androidx.core.view.WindowCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.truc.databinding.ActivityAddBinding;

public class AddActivity extends AppCompatActivity {

    private AppBarConfiguration appBarConfiguration;
    private ActivityAddBinding binding;
    public static Contact contactToAdd;
    public static long ContactId =-1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityAddBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_add);

        appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);

        Intent intent = getIntent();
        ContactId =intent.getLongExtra("id",-1);
        Log.d("DebugForm",ContactId+" from Add");


        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(contactToAdd!=null){
                    Log.d("DebugDb","contact to add: "+contactToAdd);
                    contactToAdd.push(getApplicationContext());
                    if(First2Fragment.ContactId!=-1){
                        DatabaseHelper db =new DatabaseHelper(getApplicationContext());
                        db.deleteContact(First2Fragment.ContactId);
                        db.close();
                    }
                    Intent intent1 =new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(intent1);


                }
                else{
                    Snackbar.make(view, "truc", Snackbar.LENGTH_LONG)
                            .setAnchorView(R.id.fab)
                            .setAction("Action", null).show();
                }
            }
        });
        binding.supr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(First2Fragment.ContactId!=-1){
                    DatabaseHelper db =new DatabaseHelper(getApplicationContext());
                    db.deleteContact(First2Fragment.ContactId);
                    db.close();
                    Intent intent1 =new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(intent1);
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_add);

        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }
}